package com.booking.service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.booking.models.*;
import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;

public class PrintService {
    public static void printMenu(String title, String[] menuArr) {
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);
            num++;
        }
    }

    public static String printServices(List<Service> serviceList) {
        StringBuilder result = new StringBuilder();
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result.append(service.getServiceName()).append(", \n");
        }
        return result.toString();
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showRecentReservation(List<Reservation> reservationList) {
        AsciiTable at = new AsciiTable();
        at.setTextAlignment(TextAlignment.JUSTIFIED);
        int num = 1;
        List<String> header = Arrays.asList("No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        at.addRule();
        at.addRow(header).setTextAlignment(TextAlignment.CENTER);
        at.addRule();
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting") || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                at.addRow(num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                at.addRule();
                num++;
            }
        }
        System.out.println(at.render());
    }

    public static void showAllCustomer(List<Person> personList) {
        List<String> header = Arrays.asList("No.", "ID", "Nama", "Alamat", "Membership", "Uang");
        List<Customer> customerList = personList.stream()
                .filter(person -> person instanceof Customer)
                .map(person -> (Customer) person)
                .collect(Collectors.toList());
        int num = 1;

        AsciiTable at = new AsciiTable();
        at.setTextAlignment(TextAlignment.JUSTIFIED);

        at.addRule();
        at.addRow(header).setTextAlignment(TextAlignment.CENTER);
        at.addRule();
        for (Customer customer : customerList) {
            String money =  formatCurrency(customer.getMoney());
            at.addRow(
                    num, customer.getId(), customer.getName(), customer.getAddress(), customer.getMember().getMembershipName(), money);
            num++;
        }
        at.addRule();
        System.out.println(at.render());
    }

    public static void showAllEmployee(List<Person> personList) {
        List<String> header = Arrays.asList("No.", "ID", "Nama", "Alamat", "Pengalaman");
        List<Employee> customerList = personList.stream()
                .filter(person -> person instanceof Employee)
                .map(person -> (Employee) person)
                .collect(Collectors.toList());
        int num = 1;

        AsciiTable at = new AsciiTable();
        at.setTextAlignment(TextAlignment.JUSTIFIED);

        at.addRule();
        at.addRow(header).setTextAlignment(TextAlignment.CENTER);
        at.addRule();
        for (Employee employee : customerList) {
            at.addRow(
                    num, employee.getId(), employee.getName(), employee.getAddress(), employee.getExperience());
            num++;
        }
        at.addRule();
        System.out.println(at.render());
    }

    public static void showHistoryReservation(List<Reservation> reservationList) {
        AsciiTable at = new AsciiTable();
        at.setTextAlignment(TextAlignment.JUSTIFIED);
        int num = 1;
        // Tabel
        List<String> header = Arrays.asList("No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        double total = 0;
        at.addRule();
        at.addRow(header).setTextAlignment(TextAlignment.CENTER);
        at.addRule();
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Finish")) {
                at.addRow(
                        num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                at.addRule();
                num++;
                total += reservation.getReservationPrice();
            }
        }
        System.out.println(at.render());
        // Total
        String totalKeuntungan = formatCurrency(total);
        List<String> footer = Arrays.asList("Total Keuntungan", totalKeuntungan);
        at = new AsciiTable();
        at.addRule();
        at.addRow(footer).setTextAlignment(TextAlignment.RIGHT);
        at.addRule();
        System.out.println(at.render());
    }

    public static void showAllServices(List<Service> serviceList) {
        List<String> header = Arrays.asList("No.", "ID", "Nama", "Harga");
        int num = 1;

        AsciiTable at = new AsciiTable();

        at.addRule();
        at.addRow(header).setTextAlignment(TextAlignment.CENTER);
        at.addRule();
        for (Service service : serviceList) {
            String price = formatCurrency(service.getPrice());
            at.addRow(
                    num, service.getServiceId(), service.getServiceName(), price);
            num++;
        }
        at.addRule();
        System.out.println(at.render());
    }

    public static String formatCurrency(double price) {
        return "Rp." + String.format("%,.2f", price);
    }
}
