package com.booking.service;


import com.booking.models.*;

import java.util.*;

public class ReservationService {
    public static void createReservation(List<Reservation> reservationList, List<Service> serviceList, List<Person> personList, Scanner input) {
        Customer selectedCustomer;
        Employee selectedEmployee;
        List<Service> selectedServices = new ArrayList<>();

        // Customers
        System.out.println("Customers");
        PrintService.showAllCustomer(personList);
        while (true) {
            System.out.println("Tuliskan customer ID anda:");
            String id = input.nextLine();
            try {
                selectedCustomer = getCustomerByCustomerId(personList, id);
                break;
            } catch (Exception e) {
                System.out.println("Customer ID salah/tidak ditemukan!");
            }
        }
        // Employees
        System.out.println("Employees");
        PrintService.showAllEmployee(personList);
        while (true) {
            System.out.println("Pilih pegawai dengan menuliskan ID tersebut:");
            String id = input.nextLine();
            try {
                selectedEmployee = getEmployeeByEmployeeId(personList, id);
                break;
            } catch (Exception e) {
                System.out.println("ID salah/tidak ditemukan! Silakan masukkan kembali ID pegawai:");
            }
        }
        // Services
        System.out.println("Services");
        PrintService.showAllServices(serviceList);
        while (true) {
            if (selectedServices.size() == serviceList.size()) {
                System.out.println("Service telah terpilih semua!");
                break;
            }
            System.out.println("Pilih service yang diinginkan dengan menuliskan ID tersebut:");
            String id = input.nextLine();
            boolean hasService = hasService(selectedServices, id);
            try {
                Service service = getServiceByServiceId(serviceList, id);
                if (!hasService) {
                    selectedServices.add(service);
                    String answer = answerBinaryInput(input, "(Y / N)", "\\b[YyNn]\\b");
                    if (answer.equalsIgnoreCase("n")) break;
                } else {
                    System.out.println("Service yang sama sudah dipilih. Pilih service lainnya.");
                }
            } catch (Exception e) {
                System.out.println("Service tidak ditemukan!");
            }
        }
        Reservation newReservation = new Reservation(generateId("RSV-"), selectedCustomer, selectedEmployee, selectedServices, "in process");
        reservationList.add(newReservation);

        // Total price
        String total = PrintService.formatCurrency(newReservation.getReservationPrice());
        System.out.println("Berhasil menambahkan reservasi!");
        System.out.printf("Total harga: %s\n\n", total);
    }

    public static void commitReservation(List<Reservation> reservationList, List<Person> personList, Scanner input) {
        Reservation reservation;
        PrintService.showRecentReservation(reservationList);

        while (true) {
            System.out.println("Ketik \"menu\" untuk kembali.\nSilahkan masukkan reservation ID:");
            String id = input.nextLine();
            if (id.equalsIgnoreCase("menu")) break;
            try {
                reservation = getReservationByReservationId(reservationList, id);
                String stateWorkstage = reservation.getWorkstage();
                switch (stateWorkstage.toLowerCase()) {
                    case "in process":
                        System.out.println("Ketikkan \"Finish\" untuk menyelesaikan transaksi, atau \"Cancel\" ");
                        String answer = input.nextLine();

                        if (answer.equalsIgnoreCase("finish")) {
                            changeWorkstage(reservationList, reservation, "Finish");
                            calcPayment(personList, reservation);
                            System.out.printf("Transaksi reservation %s berhasil!\n", reservation.getReservationId());
                        } else {
                            System.out.println("Membatalkan proses transaksi reservation.");
                            break;
                        }
                        break;
                    case "finish":
                        System.out.print("Reservation yang dicari sudah selesai!\n");
                        break;
                }

            } catch (Exception e) {
                System.out.println("Reservation ID salah / tidak ditemukan.");
            }
        }
    }

    private static void calcPayment(List<Person> personList, Reservation reservation) {
        double totalPayment = reservation.getReservationPrice();
        for (Person e : personList) {
            if (e instanceof Customer) {
                boolean equals = e.getId().equalsIgnoreCase(reservation.getCustomer().getId());
                if (equals) {
                    double currentMoney = ((Customer) e).getMoney();
                    ((Customer) e).setMoney(currentMoney - totalPayment);
                    break;
                }
            }
        }
    }

    private static void changeWorkstage(List<Reservation> reservationList, Reservation reservation, String stateWorkstage) {
        for (Reservation e : reservationList) {
            boolean equals = e.getReservationId().equalsIgnoreCase(reservation.getReservationId());
            if (equals) {
                e.setWorkstage(stateWorkstage);
                break;
            }
        }
    }

    public static Customer getCustomerByCustomerId(List<Person> personList, String id) {
        return (Customer) personList.stream()
                .filter(person -> person instanceof Customer)
                .filter(customer -> customer.getId().equalsIgnoreCase(id))
                .findFirst().orElseThrow();
    }

    private static Employee getEmployeeByEmployeeId(List<Person> personList, String id) {
        return (Employee) personList.stream()
                .filter(person -> person instanceof Employee)
                .filter(customer -> customer.getId().equalsIgnoreCase(id))
                .findFirst().orElseThrow();
    }

    private static Service getServiceByServiceId(List<Service> serviceList, String id) {
        return serviceList.stream()
                .filter(service -> service.getServiceId().equalsIgnoreCase(id))
                .findFirst().orElseThrow();
    }

    private static Reservation getReservationByReservationId(List<Reservation> reservationList, String id) {
        return reservationList.stream()
                .filter(reservation -> reservation.getReservationId().equalsIgnoreCase(id))
                .findFirst().orElseThrow();
    }

    private static boolean hasService(List<Service> selectedServices, String id) {
        return selectedServices.stream()
                .anyMatch(service -> service.getServiceId().equalsIgnoreCase(id));
    }

    private static String generateId(String custom) {
        Random random = new Random();
        int calc = Math.abs(random.nextInt());
        return custom + calc;
    }

    private static String answerBinaryInput(Scanner input, String answer, String regex) {
        while (true) {
            System.out.printf("Tambah service lain? %s\n", answer);
            String in = input.nextLine();
            if (in.matches(regex)) {
                return in;
            } else {
                System.out.printf("Jawaban salah. Ketik %s\n", answer);
            }
        }
    }

// Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
