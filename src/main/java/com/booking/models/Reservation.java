package com.booking.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;
    //   workStage (In Process, Finish, Canceled)

    public Reservation(String reservationId, Customer customer, Employee employee, List<Service> services, String workstage) {
        this.reservationId = reservationId;
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.reservationPrice = calculateReservationPrice(services);
        this.workstage = workstage;
    }

    private double calculateReservationPrice(List<Service> serviceList) {
        double result = 0;
        double discount = 0;
        String member = this.customer.getMember().getMemberId();
        switch (member) {
            case "Mem-01":
                discount = 0;
                break;
            case "Mem-02":
                discount = 5;
                break;
            case "Mem-03":
                discount = 10;
                break;
        }
        for (Service service : serviceList) {
            result += service.getPrice();
        }
        result = result - (result * discount / 100);
        return result;
    }
}
